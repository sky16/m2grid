<?php /** * Copyright © 2013-2017 Magento, Inc. All rights reserved. * See COPYING.txt for license details. */ 
namespace Codextblog\Customgridtab\Ui\DataProvider\Product; 
use Magento\Framework\App\RequestInterface; 
use Magento\Ui\DataProvider\AbstractDataProvider; 
use Iverve\Tireprice\Model\ResourceModel\Tierprice\Collection; 
use Iverve\Tireprice\Model\ResourceModel\Tierprice; 
//use Iverve\Tireprice\Model\Tierprice; /** * Class CustomDataProvider * * @method Collection getCollection */ 
class CustomDataProvider extends AbstractDataProvider { /** * @var CollectionFactory */ 
    protected $collectionFactory; /** * @var RequestInterface */ 
    protected $request; /** * @param string $name * @param string $primaryFieldName * @param string $requestFieldName * @param CollectionFactory $collectionFactory * @param RequestInterface $request * @param array $meta * @param array $data */ public function __construct( $name, $primaryFieldName, $requestFieldName, Collection $collectionFactory, RequestInterface $request, array $meta = [], array $data = [] ) { 
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data); 
        $this->collectionFactory = $collectionFactory;
        $this->collection = $this->collectionFactory->_construct();
        $this->request = $request;
    }
 
    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $this->getCollection();
 
        $arrItems = [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => [],
        ];
 
        foreach ($this->getCollection() as $item) {

            $arrItems['items'][] = $item->toArray([]);
        }
 
        return $arrItems;
    }
 
    /**
     * {@inheritdoc}
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        $field = $filter->getField();
 
        if (in_array($field, ['id','title','created_at', 'is_active'])) {
            $filter->setField($field);
        }
 
 
 
        parent::addFilter($filter);
    }
}