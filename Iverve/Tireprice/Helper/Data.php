<?php
/**
 * Copyright © 2015 Iverve . All rights reserved.
 */
namespace Iverve\Tireprice\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	public function __construct(\Magento\Framework\App\Helper\Context $context
	) {
		parent::__construct($context);
	}
}