<?php
namespace Iverve\Tireprice\Block\Adminhtml;
class Tierprice extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_tierprice';/*block grid.php directory*/
        $this->_blockGroup = 'Iverve_Tireprice';
        $this->_headerText = __('Tierprice');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();
		
    }
}
