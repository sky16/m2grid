<?php
namespace Iverve\Tireprice\Block\Adminhtml\Tierprice\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_tierprice_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Tierprice Information'));
    }
}