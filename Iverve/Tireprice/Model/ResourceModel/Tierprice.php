<?php
/**
 * Copyright © 2015 Iverve. All rights reserved.
 */
namespace Iverve\Tireprice\Model\ResourceModel;

/**
 * Tierprice resource
 */
class Tierprice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('tireprice_tierprice', 'id');
    }

  
}
