<?php
/**
 * Copyright © 2015 Iverve. All rights reserved.
 */
namespace Iverve\Tierprice\Model\ResourceModel;

/**
 * Gridmodel resource
 */
class Gridmodel extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('tierprice_gridmodel', 'id');
    }

  
}
