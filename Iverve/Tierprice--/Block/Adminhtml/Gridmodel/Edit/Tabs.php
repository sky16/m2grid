<?php
namespace Iverve\Tierprice\Block\Adminhtml\Gridmodel\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_gridmodel_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Gridmodel Information'));
    }
}