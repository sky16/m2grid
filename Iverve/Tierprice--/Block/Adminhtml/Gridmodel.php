<?php
namespace Iverve\Tierprice\Block\Adminhtml;
class Gridmodel extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_gridmodel';/*block grid.php directory*/
        $this->_blockGroup = 'Iverve_Tierprice';
        $this->_headerText = __('Gridmodel');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();
		
    }
}
